# Containerizer

## Container

```


[ u8 ][ []u8 ]
  |      |
  |      \ (B) Body
  |
  \ (A) Encoding

```

* (A) **Encocing** -- type of encoding
	* **0**  -- Raw data
	* **1**  -- Raw data + Length is [4]u8
	* **2**  -- Raw data + Length [8]u8
	* **3**  -- Is Pocket
	* **4**  -- List
	* **5**  -- Key Value
	* **32** -- Deflate
	* **64** -- AES

---

## Pocket

```

[ u8 ][ []u8 ][ []u8 ]
  |       |       |
  |       |       \ (C) Body
  |       |
  |       \ (B) Meta
  |
  \ (A) Type

```

### (A) Type

First byte is a *logical* type of the pocket.

Values:

* **0**  -- Raw data
* **46** -- Data for some service
* **47** -- Recovery dump data
* **48** -- Streaming data
* **49** -- Files data

### (B) Meta

Is container (type 1 or 2) with specific data.
If type of container is 0, then it's empty.

Examples

* `Container1(Container0(Data))`
* `Container1(Container3(Pocket2(Key, Value)))`


### (C) Body

Is container (type 1 or 2) with specific data.


---

## Tests for Containers and Pockets

```rust

use containerizer::{
	structs::{ List, KeyValue },
	raw::{ Raw, RawLen4, RawLen8 },
	pocket::{ Pocket },
	encodings::{ Base64 },
	compression::{ Deflate },
	cryptography::{ Aes }
};

assert_eq!(RawLen4::encode(&vec![1,2,3,4,5]), vec![1,5,0,0,0,1,2,3,4,5]);
//                                                 | |_____| |_______|
//                                                 |    |        |
//                                                 |    |        \ Body
//                                                 |    |
//                                                 |    \ Length of body
//                                                 |
//                                                 \ Type

assert_eq!(RawLen4::decode(&vec![1,5,0,0,0,1,2,3,4,5]), vec![1,2,3,4,5]);

// ---

let res = Pocket::encode(0u8, Raw::encode(vec![]), RawLen4::encode(vec![1,2,3]));
//                       |        |                         |
//                       |        |                         \ Body is Raw4 container
//                       |        |
//                       |        \ Meta is empty Raw container
//                       |
//                       \ Type of pocket

assert_eq!(Pocket::decode(&res), (0u8, vec![], vec![1,3,0,0,0,1,2,3]) );

let (t, meta, body) = Pocket::decode(&res);

let meta = Raw::decode(&meta);

let body = RawLen4::decode(&body);

```