
#[test]
fn raw() {
	use super::Container;
	use super::raw::Raw;
	use super::codes::CODE_RAW;
	assert_eq!(Raw::encode(vec![1,2,3,4,5]).unwrap(), vec![CODE_RAW,1,2,3,4,5]);
	//                                                       |      |_______|
	//                                                       |          |
	//                                                       |          \ Body
	//                                                       |
	//                                                       \ Type

	assert_eq!(Raw::decode(vec![CODE_RAW,1,2,3,4,5]).unwrap(), vec![1,2,3,4,5]);
}


#[test]
fn raw4() {
	use super::Container;
	use super::raw::RawLen32;
	use super::codes::CODE_RAWLEN32;
	assert_eq!(RawLen32::encode(vec![1,2,3,4,5]).unwrap(), vec![CODE_RAWLEN32,5,0,0,0,1,2,3,4,5]);
	//                                                               |      |_____| |_______|
	//                                                               |         |        |
	//                                                               |         |        \ Body
	//                                                               |         |
	//                                                               |         \ Length of body
	//                                                               |
	//                                                               \ Type

	assert_eq!(RawLen32::decode(vec![CODE_RAWLEN32,5,0,0,0,1,2,3,4,5]).unwrap(), vec![1,2,3,4,5]);
}


#[test]
fn raw8() {
	use super::Container;
	use super::raw::RawLen64;
	use super::codes::CODE_RAWLEN64;
	assert_eq!(RawLen64::encode(vec![1,2,3,4,5]).unwrap(), vec![CODE_RAWLEN64,5,0,0,0,0,0,0,0,1,2,3,4,5]);
	//                                                               |      |_____________| |_______|
	//                                                               |         |               |
	//                                                               |         |               \ Body
	//                                                               |         |
	//                                                               |         \ Length of body
	//                                                               |
	//                                                               \ Type

	assert_eq!(RawLen64::decode(vec![CODE_RAWLEN64,5,0,0,0,0,0,0,0,1,2,3,4,5]).unwrap(), vec![1,2,3,4,5]);
}


#[test]
fn pocket() {
	use super::{
		Container,
		pocket::{ Pocket }
	};

	let res = Pocket::encode((0u8, vec![], vec![1,2,3])).unwrap();
	//                         |     |           |
	//                         |     |           \ Body
	//                         |     |
	//                         |     \ Meta is empty Raw container
	//                         |
	//                         \ Type of pocket
	assert_eq!(Pocket::decode(res).unwrap(), (0u8, vec![], vec![1,2,3]) );

	let res = Pocket::encode((0u8, vec![1,2,3], vec![1,2,3])).unwrap();
	assert_eq!(Pocket::decode(res).unwrap(), (0u8, vec![1,2,3], vec![1,2,3]) );
}


#[test]
fn list() {
	use super::Container;
	use super::structs::List;
	use super::codes::{ CODE_LIST, CODE_RAWLEN32, CODE_RAW };

	let data = vec![vec![1,2,3,4], vec![5,6]];
	let res = vec![CODE_LIST,CODE_RAWLEN32,4,0,0,0,1,2,3,4,CODE_RAWLEN32,2,0,0,0,5,6];
	assert_eq!(List::encode(data.clone()).unwrap(), res);
	assert_eq!(List::decode(res).unwrap(), data);

	let data = vec![vec![], vec![5,6]];
	let res = vec![CODE_LIST,CODE_RAW,CODE_RAWLEN32,2,0,0,0,5,6];
	assert_eq!(List::encode(data.clone()).unwrap(), res);
	assert_eq!(List::decode(res).unwrap(), data);
}
