
use super::{ Container, Result };

use super::errors::{ ERR_WRONG_CODE };

use super::codes::{ CODE_POCKET, CODE_RAW, CODE_RAWLEN32, CODE_RAWLEN64 };

use super::raw::{ Raw, RawLen32, RawLen64 };

pub struct Pocket;

type Args = (u8, Vec<u8>, Vec<u8>);

impl Container<Args, Vec<u8>> for Pocket {
	fn code() -> u8 {
		CODE_POCKET
	}
	fn encode(data: Args) -> Result<Vec<u8>> {
		let (code, meta, body) = data;
		let mut res = Vec::new();
		res.push(Pocket::code());
		res.push(code);
		if meta.len() == 0 {
			res.extend(Raw::encode(meta)?);
		} else if meta.len()+1 < u32::max_value() as usize {
			res.extend(RawLen32::encode(meta)?);
		} else {
			res.extend(RawLen64::encode(meta)?);
		}
		res.extend(body);
		Ok(res)
	}
	fn decode(data: Vec<u8>) -> Result<Args> {
		if data[0] != Pocket::code() {
			return Err(ERR_WRONG_CODE)
		}
		let code = data[1];
		let mut margin = 3;
		let meta = match data[2] {
			CODE_RAW => Vec::new(),
			CODE_RAWLEN32 => {
				let len = RawLen32::len(&data[3..7].to_vec());
				margin += 4 + len;
				RawLen32::decode(data[2..margin].to_vec())?
			},
			CODE_RAWLEN64 => {
				let len = RawLen64::len(&data[3..11].to_vec());
				margin += 8 + len;
				RawLen64::decode(data[3..margin].to_vec())?
			},
			_ => return Err(ERR_WRONG_CODE),
		};
		let body = data[margin..].to_vec();
		Ok((code, meta, body))
	}
}
