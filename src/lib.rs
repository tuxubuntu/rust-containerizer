
type Result<T> = std::result::Result<T, &'static str>;

pub trait Container<T, F> {
	fn code() -> u8;
	fn encode(T) -> Result<F>;
	fn decode(F) -> Result<T>;
}

mod codes;
mod errors;

pub mod structs;

pub mod raw;

pub mod pocket;

pub mod encodings {
	pub struct Base64;
}

pub mod compression {
	pub struct Deflate;
}

pub mod cryptography {
	pub struct Aes;
}


#[cfg(test)]
mod tests;
