
use super::{ Container, Result };

use super::errors::{ ERR_WRONG_CODE };

use super::codes::{ CODE_RAW, CODE_RAWLEN32, CODE_RAWLEN64 };

pub struct Raw;

pub struct RawLen32;

pub struct RawLen64;


impl Container<Vec<u8>, Vec<u8>> for Raw {
	fn code() -> u8 {
		CODE_RAW
	}
	fn encode(data: Vec<u8>) -> Result<Vec<u8>> {
		let mut res = Vec::new();
		res.push(Raw::code());
		res.extend(data);
		Ok(res)
	}
	fn decode(data: Vec<u8>) -> Result<Vec<u8>> {
		if data[0] != Raw::code() {
			return Err(ERR_WRONG_CODE)
		}
		Ok(data[1..].to_vec())
	}
}


impl Container<Vec<u8>, Vec<u8>> for RawLen32 {
	fn code() -> u8 {
		CODE_RAWLEN32
	}
	fn encode(data: Vec<u8>) -> Result<Vec<u8>> {
		let mut res = Vec::new();
		res.push(RawLen32::code());
		{
			use std::mem::transmute;
			let bytes: [u8; 4] = unsafe { transmute((data.len() as u32).to_le()) };
			res.extend(bytes.to_vec());
		};
		res.extend(data);
		Ok(res)
	}
	fn decode(data: Vec<u8>) -> Result<Vec<u8>> {
		if data[0] != RawLen32::code() {
			return Err(ERR_WRONG_CODE)
		}
		let len = RawLen32::len(&data[1..5]);
		Ok(data[5..(len+5)].to_vec())
	}
}

impl RawLen32 {
	pub fn len(data: &[u8]) -> usize {
		use std::mem::transmute;
		let mut a: [u8; 4] = Default::default();
		a.copy_from_slice(data);
		let len: u32 = unsafe { transmute(a) };
		len as usize
	}
}


impl Container<Vec<u8>, Vec<u8>> for RawLen64 {
	fn code() -> u8 {
		CODE_RAWLEN64
	}
	fn encode(data: Vec<u8>) -> Result<Vec<u8>> {
		let mut res = Vec::new();
		res.push(RawLen64::code());
		{
			use std::mem::transmute;
			let bytes: [u8; 8] = unsafe { transmute((data.len() as u64).to_le()) };
			res.extend(bytes.to_vec());
		};
		res.extend(data);
		Ok(res)
	}
	fn decode(data: Vec<u8>) -> Result<Vec<u8>> {
		if data[0] != RawLen64::code() {
			return Err(ERR_WRONG_CODE)
		}
		let len = RawLen64::len(&data[1..9]);
		Ok(data[9..(len+9)].to_vec())
	}
}

impl RawLen64 {
	pub fn len(data: &[u8]) -> usize {
		use std::mem::transmute;
		let mut a: [u8; 8] = Default::default();
		a.copy_from_slice(data);
		let len: u64 = unsafe { transmute(a) };
		len as usize
	}
}

