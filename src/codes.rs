pub const CODE_RAW: u8 = 0; // Raw data
pub const CODE_RAWLEN32: u8 = 1; // Raw data + Length is [4]u8
pub const CODE_RAWLEN64: u8 = 2; // Raw data + Length [8]u8
pub const CODE_LIST: u8 = 3; // List
pub const CODE_POCKET: u8 = 4; // Is Pocket