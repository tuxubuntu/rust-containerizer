
use super::{ Container, Result };

use super::errors::{ ERR_WRONG_CODE };

use super::codes::{ CODE_LIST, CODE_RAW, CODE_RAWLEN32, CODE_RAWLEN64 };

use super::raw::{ Raw, RawLen32, RawLen64 };

pub struct List;

pub struct KeyValue;

impl Container<Vec<Vec<u8>>, Vec<u8>> for List {
	fn code() -> u8 {
		CODE_LIST
	}
	fn encode(data: Vec<Vec<u8>>) -> Result<Vec<u8>> {
		let mut res = Vec::new();
		res.push(List::code());
		for item in data {
			if item.len() == 0 {
				res.extend(Raw::encode(item)?);
			} else if item.len()+1 < u32::max_value() as usize {
				res.extend(RawLen32::encode(item)?);
			} else {
				res.extend(RawLen64::encode(item)?);
			}
		}
		Ok(res)
	}
	fn decode(data: Vec<u8>) -> Result<Vec<Vec<u8>>> {
		if data[0] != List::code() {
			return Err(ERR_WRONG_CODE)
		}
		let mut res = Vec::new();
		let mut margin = 1;
		while margin < data.len() {
			let body = match data[margin] {
				CODE_RAW => {
					margin += 1;
					Vec::new()
				},
				CODE_RAWLEN32 => {
					let size = 4;
					let len = RawLen32::len(&data[(margin+1)..(size+margin+1)].to_vec());
					let old = margin;
					margin += size + len + 1;
					RawLen32::decode(data[old..margin].to_vec())?
				},
				CODE_RAWLEN64 => {
					let size = 8;
					let len = RawLen64::len(&data[(margin+1)..(size+margin+1)].to_vec());
					let old = margin;
					margin += size + len + 1;
					RawLen64::decode(data[old..margin].to_vec())?
				},
				_ => return Err(ERR_WRONG_CODE),
			};
			res.push(body);
		}
		Ok(res)
	}
}



